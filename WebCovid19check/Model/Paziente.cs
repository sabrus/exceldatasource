﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCovid19check.Model
{
    public class Paziente
    {
        public string Numero { get; set; }
         public string Cognome { get; set; }
        public string Nome { get; set; }
        public string DataNascita { get; set; }
        public string Comune { get; set; }
        public string Indirizzo { get; set; }
        public string Asintomatici { get; set; }
        public string Sintomatici { get; set; }
        public string Ricoverati { get; set; }
        public string RicoveratiTerapiaIntensiva { get; set; }
        public string Ospedale { get; set; }
        public string Isolamento { get; set; }
        public string TotaleGuaritiClinicamente { get; set; }
        public string TotaleGuariti { get; set; }
        public string TotaliDeceduti { get; set; }
        public string PresenzaPatologie { get; set; }
        public string TotaleCasiPositivi { get; set; }
        public string Note { get; set; }

        public string ToHtmlTable()
        {
            return $"<tr><td class='rlabel'>Numero</td><td class='rval'>{Numero}</td>" +
                $"<tr><td class='rlabel'>Cognome</td><td class='rval'>{Cognome}</td>" +
                $"<tr><td class='rlabel'>Nome</td><td class='rval'>{Nome}</td>" +
                $"<tr><td class='rlabel'>Data Nascita</td><td class='rval'>{DataNascita}</td>" +
                $"<tr><td class='rlabel'>Comune</td><td class='rval'>{Comune}</td>" +
                $"<tr><td class='rlabel'>Indirizzo</td><td class='rval'>{Indirizzo}</td>" +
                $"<tr><td class='rlabel'>Asintomatici</td><td class='rval'>{Asintomatici}</td>" +
                $"<tr><td class='rlabel'>Sintomatici (Febbre o tosse o dispnea)</td><td class='rval'>{Sintomatici}</td>" +
                $"<tr><td class='rlabel'>Ricoverati (NON in terapia intensiva)</td><td class='rval'>{Ricoverati}</td>" +
                $"<tr><td class='rlabel'>Ricoverati in Terapia intensiva</td><td class='rval'>{RicoveratiTerapiaIntensiva}</td>" +
                $"<tr><td class='rlabel'>Ospedale in cui sono ricoverati</td><td class='rval'>{Ospedale}</td>" +
                $"<tr><td class='rlabel'>Isolamento domiciliare</td><td class='rval'>{Isolamento}</td>" +
                $"<tr><td class='rlabel'>TOTALE GUARITI CLINICAMENTE </td><td class='rval'>{TotaleGuaritiClinicamente}</td>" +
                $"<tr><td class='rlabel'>TOTALE GUARITI </td><td class='rval'>{TotaleGuariti}</td>" +
                $"<tr><td class='rlabel'>TOTALI DECEDUTI (totali decessi legati al SARS Cov 2) </td><td class='rval'>{TotaliDeceduti}</td>" +
                $"<tr><td class='rlabel'>Presenza di patologie concomitanti (SI/NO)</td><td class='rval'>{PresenzaPatologie}</td>" +
                $"<tr><td class='rlabel'>TOTALE CASI POSITIVI</td><td class='rval'>{TotaleCasiPositivi}</td>" +
                $"<tr><td class='rlabel'>Note</td><td class='rval'>{Note}</td>";
        }

        public override string ToString()
        {
            return $"{{{nameof(Numero)}={Numero}, {nameof(Cognome)}={Cognome}, {nameof(Nome)}={Nome}, {nameof(DataNascita)}={DataNascita}, {nameof(Comune)}={Comune}, {nameof(Indirizzo)}={Indirizzo}, {nameof(Asintomatici)}={Asintomatici}, {nameof(Sintomatici)}={Sintomatici}, {nameof(Ricoverati)}={Ricoverati}, {nameof(RicoveratiTerapiaIntensiva)}={RicoveratiTerapiaIntensiva}, {nameof(Ospedale)}={Ospedale}, {nameof(Isolamento)}={Isolamento}, {nameof(TotaleGuaritiClinicamente)}={TotaleGuaritiClinicamente}, {nameof(TotaleGuariti)}={TotaleGuariti}, {nameof(TotaliDeceduti)}={TotaliDeceduti}, {nameof(PresenzaPatologie)}={PresenzaPatologie}, {nameof(TotaleCasiPositivi)}={TotaleCasiPositivi}, {nameof(Note)}={Note}}}";
        }
    }
}
