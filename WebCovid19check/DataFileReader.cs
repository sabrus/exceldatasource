﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCovid19check.Model;

namespace WebCovid19check
{
    public class DataFileReader : IDataFileReader
    {
        private readonly ILogger<DataFileReader> _logger;
        protected readonly IConfiguration _configuration;
        private List<Paziente> Pazienti;


        public DataFileReader(ILogger<DataFileReader> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;

            ReadExcelFile();
        }


        void ReadExcelFile()
        {
            try
            {
                Pazienti = new List<Paziente>();
                using SpreadsheetDocument doc = SpreadsheetDocument.Open(_configuration.GetValue<string>("File"), false);
                WorkbookPart workbookPart = doc.WorkbookPart;
                Sheets thesheetcollection = workbookPart.Workbook.GetFirstChild<Sheets>();
                StringBuilder excelResult = new StringBuilder();

                if (thesheetcollection.Count() > 0)
                {
                    Sheet thesheet = (Sheet)thesheetcollection.First();
                    Worksheet theWorksheet = ((WorksheetPart)workbookPart.GetPartById(thesheet.Id)).Worksheet;

                    SheetData thesheetdata = (SheetData)theWorksheet.GetFirstChild<SheetData>();
                    foreach (Row thecurrentrow in thesheetdata)
                    {
                        if (thecurrentrow.RowIndex == 1)
                        {
                            continue;
                        }

                        int cellIdx = 0;
                        Paziente current = new Paziente();
                        foreach (Cell thecurrentcell in thecurrentrow)
                        {
                            cellIdx++;
                            Object cellValue = null;

                            string currentcellvalue = string.Empty;
                            if (thecurrentcell.DataType != null)
                            {
                                if (thecurrentcell.DataType == CellValues.SharedString)
                                {
                                    int id;
                                    if (Int32.TryParse(thecurrentcell.InnerText, out id))
                                    {
                                        SharedStringItem item = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
                                        if (item.Text != null)
                                        {
                                            //code to take the string value  
                                            cellValue = item.Text.Text;
                                        }
                                        else if (item.InnerText != null)
                                        {
                                            currentcellvalue = item.InnerText;
                                        }
                                        else if (item.InnerXml != null)
                                        {
                                            currentcellvalue = item.InnerXml;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (cellIdx == 4)
                                {
                                    if (double.TryParse(thecurrentcell.InnerText, out var cellDouble))
                                    {
                                        cellValue = DateTime.FromOADate(cellDouble).ToString("dd/MM/yyyy");
                                    }

                                }
                                else
                                {
                                    cellValue = thecurrentcell.InnerText;
                                }

                            }

                            if (cellValue is string)
                            {
                                cellValue = ((string)cellValue).Trim();
                            }

                            switch (cellIdx)
                            {
                                case 1:
                                    current.Numero = (string)cellValue;
                                    break;
                                case 2:
                                    current.Cognome = (string)cellValue;
                                    break;
                                case 3:
                                    current.Nome = (string)cellValue;
                                    break;

                                case 4:
                                    current.DataNascita = (string)cellValue;
                                   
                                    break;

                                case 5:
                                    current.Comune = (string)cellValue;
                                    break;

                                case 6:
                                    current.Indirizzo = (string)cellValue;
                                    break;

                                case 7:
                                    current.Asintomatici = (string)cellValue;
                                    break;

                                case 8:
                                    current.Sintomatici = (string)cellValue;
                                    break;

                                case 9:
                                    current.Ricoverati = (string)cellValue;
                                    break;

                                case 10:
                                    current.RicoveratiTerapiaIntensiva = (string)cellValue;
                                    break;

                                case 11:
                                    current.Ospedale = (string)cellValue;
                                    break;

                                case 12:
                                    current.Isolamento = (string)cellValue;
                                    break;

                                case 13:
                                    current.TotaleGuaritiClinicamente = (string)cellValue;
                                    break;

                                case 14:
                                    current.TotaleGuariti = (string)cellValue;
                                    break;

                                case 15:
                                    current.TotaliDeceduti = (string)cellValue;
                                    break;

                                case 16:
                                    current.PresenzaPatologie = (string)cellValue;
                                    break;

                                case 17:
                                    current.TotaleCasiPositivi = (string)cellValue;
                                    break;

                                case 18:
                                    current.Note = (string)cellValue;
                                    break;
                            }
                        }
                        Pazienti.Add(current);
                        _logger.LogInformation(current.ToString());
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Errore {e.Message}");
                throw;
            }
        }

        public Paziente GetByData(string nome, string cognome, string dataNascita)
        {
            return Pazienti.Where(p => p.Nome.Equals(nome, StringComparison.InvariantCultureIgnoreCase)
                && p.Cognome.Equals(cognome, StringComparison.InvariantCultureIgnoreCase)
                && p.DataNascita.Equals(dataNascita)).FirstOrDefault();
        }


    }


}
