﻿using System;
using WebCovid19check.Model;

namespace WebCovid19check
{
    public interface IDataFileReader
    {
        Paziente GetByData(string nome, string cognome, string dataNascita);
    }
}