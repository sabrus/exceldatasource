﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebCovid19check.Model;

namespace WebCovid19check.Controllers
{
    [Route("api/search")]
    [ApiController]
    [EnableCors("All")]
    public class SearchController : ControllerBase
    {
        private readonly ILogger<SearchController> _logger;
        private IDataFileReader _dataFileReader;
        public SearchController(ILogger<SearchController> logger, IDataFileReader dataFileReader)
        {
            _logger = logger;
            _dataFileReader = dataFileReader;
        }

        [HttpGet]
        public async Task<IActionResult> GetByData(string nome, string cognome, string dataNascita)
        {
            _logger.LogInformation($"Ricerca {cognome} - {nome} - {dataNascita}");
            var res = _dataFileReader.GetByData(nome, cognome, dataNascita);
            if (res == null)
            {
                _logger.LogInformation($"KO - Nessun risultato trovato");
                return NotFound("Nessun risultato trovato");
            }
            _logger.LogInformation($"OK {res}");
            return Ok(res.ToHtmlTable());
        }
    }
}